//
//  main.cpp
//  distribution
//
//  Created by Eugene Yurin on 11/04/2019.
//  Copyright © 2019 Eugene Yurin. All rights reserved.
//

#include <fstream>
#include <vector>
#include <algorithm>
#include <math.h>
using namespace std;

const unsigned MIN_DATA = 1600;
const unsigned MIN_DIST = 4;
const unsigned DIVIDER = 10;

struct XY;
vector <double> data;
vector <XY> distribution;

struct XY
{
    double X;
    double Y;
    friend ostream &operator << (ostream &out, const XY &xy)
    {
        out << xy.X << '\t' << xy.Y << '\n';
        return out;
    }
};


int input()
{
    ifstream file("input.txt");
    if (!file)
        return 1;
    
    double x;
    while(file >> x)
        data.push_back(x);
    
    file.close();
    data.shrink_to_fit();
    if (data.size() < MIN_DATA)
        return 1;
    
    return 0;
}

int calculate()
{
    distribution.resize((size_t)sqrt(data.size())/DIVIDER);
    if (distribution.size() < MIN_DIST)
        return 2;
    
    sort(data.begin(),data.end());
    
    double step = data[data.size()-1] - data[0];
    if (!step)
        return 2;
    step = step / ((int)distribution.size()-2);
    distribution[0].X=data[0] + step/2;
    for (int i = 1; i < distribution.size(); i++)
        distribution[i].X = distribution[i-1].X + step;
    
    auto it = data.begin();
    auto it_ = it;

    for (auto &dis : distribution)
    {
        it = lower_bound(it, data.end(), dis.X);
        dis.Y = (double)distance(it_, it)/step/((double)data.size());
        it_ = it;
        dis.X -= step/2;
    }
    return 0;
}

int output()
{
    ofstream file("output.txt");
    if (!file)
        return 3;
    
    for (auto &dis : distribution)
        file << dis;
    
    file.close();
    return 0;
}

int main()
{
    if (input())
        return 1;
    if (calculate())
        return 2;
    if (output())
        return 3;
    return 0;
}
