# Distribution
The program calculates probability density for the "input.txt" file data.

The function is normalized by unit.
## Program description
### Source
Project contains one source file - main.cpp with following functions:

* main - calls input, calculation and output functions. Returns 1/2/3 if error

    * input - reads data from "input.txt". Returns 1 if error

    * calculate - restores distribution by data. Return 2 if error

    * output - writes distribution function in "output.txt". Return 3 if error

## Remarks about input data
### 1
This version of the program is designed to obtain sufficiently accurate functions

    -it means that input data should contain much more than 1 000 numbers (~ 100 000).
### 2
The example of input.txt is attached to the project

    -this file contains data which respond to the normal distribution